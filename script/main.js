"use strict";

const iconPass = document.querySelectorAll(".fa-eye");
const inputs = document.querySelectorAll("input");
const submitBtn = document.querySelector("button");
const form = document.querySelector("form")
const invalidInputsParagraph = document.createElement("p");
iconPass.forEach((elem) => {
  elem.addEventListener("click", function (e) {
    e.target.classList.toggle("fa-eye-slash");
    e.target.classList.toggle("fa-eye");
    if (e.target.classList.contains("fa-eye-slash")) {
      e.target.previousElementSibling.type = "text";
    } else {
      e.target.previousElementSibling.type = "password";
    }
  });
});
submitBtn.addEventListener("click", function checkInputValue(e) {
  e.preventDefault()
  const arr = Array.from(inputs);
  let firstInput = arr[0];
  let secondInput = arr[1];
  if (firstInput.value !== secondInput.value) {
    invalidInputsParagraph.innerText = "Потрібно ввести однакові значення";
    invalidInputsParagraph.style.color = "red";
    document.body.append(invalidInputsParagraph);
  } else {
    alert("You are welcome!");
    invalidInputsParagraph.remove()
  }
});
